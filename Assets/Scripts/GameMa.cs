﻿using UnityEngine;
using System.Collections;

public class GameMa : MonoBehaviour {

	public static bool GameIsOver;

	public GameObject gameOverUI;

	void Start ()
	{
		GameIsOver = false;
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.E))
		{
			EndGame();
		}
		if (GameIsOver)
			return;

		if (PlayerStats.Lives <= 0)
		{
			EndGame();
		}
	}

	void EndGame ()
	{
		GameIsOver = true;
		gameOverUI.SetActive(true);
	}

}

